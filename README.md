# 天理猫协

# 小程序帮助
## 介绍
开源天理猫协的源代码，在北大猫协的基础上加以修改

## 软件架构
使用了云开发技术


## 安装教程

1.  将项目克隆到本地后，解压项目，导入到微信开发者工具
2.  申请云开发空间
![Image text](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/capture_20221020234455548.bmp)
3.  只保留图片中cloudfunctions文件夹下的，其他的删除
![Image text](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/capture_20221020234036107.bmp)
## 使用说明
1.  在云开发空间内开通内容管理
![Image text](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/capture_20221020234518274.bmp)
2.  在申请开通内容管理后，点击导入模型，导入内容管理模型下的 json文件
![Image text](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/capture_20221020234102893.bmp)

3.  之后就可以添加小动物了
![Image text](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/capture_20221020235922936.bmp)
## 参与贡献

1.  Tom
2.  codecat


## 附录
![小程序码](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/gh_b6600eb1faea_430.jpg)
![QQ群](https://gitee.com/lovecrx/tianli-cat-association/raw/master/images/joinus.jpg)