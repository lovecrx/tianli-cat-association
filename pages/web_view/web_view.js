Page({
    data:{

    },
    onLoad(event){
        console.log(event._id)
        this.getanimalsdetail(event._id)
    },
    getanimalsdetail(_id){
        var that = this
        this.myShowToast('加载中');
        wx.cloud.callFunction({
          name: 'getstory',
          data:{
            _id:_id
          }
        }).then(res=>{
            that.setData({
                animals:res.result.data[0]
            })
        })
      },
      myShowToast(title = '标题') {
        wx.showToast({
          title: title,
          image:'/pages/images/loading.gif',
          size: 700,
          mask: true
        })
      },
})