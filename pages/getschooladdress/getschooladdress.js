
Page({
    data: {
        schoolTextSchool:'选择您所在的校区',
    },
    onLoad(){
        this.getaddress()
    },
    inputSearch: function(o) {
        this.data.pageId = 1, this.data.schoolList = [], this.setData({
            searchVal: o.detail.value,
            flag: !0
        }), this.getaddress();
    },
    finishSumbit: function() {
        this.data.pageId = 1, this.data.schoolList = [], this.getaddress();
    },
    choiceAdd: function(a) {
        console.log('a',a)
        var that = this, i = a.currentTarget.dataset.cityindex, e = a.currentTarget.dataset.shoolindex,
        n = that.data.schoolList[i].schoolInfos[e],
         l = {
            school_id: n.school_id,
            school_name: n.school_name,
            school_logo: n.school_logo,
            school_sos_phone: n.school_sos_phone,
            school_qq: n.school_qq,
            school_wechat: n.school_wechat
        };
        wx.setStorageSync("school", l);
        wx.switchTab({
            url: "/pages/index/index"
        })

    },
    getaddress(){
        var that = this
        this.myShowToast('加载中');
        wx.cloud.callFunction({
            name: 'getaddress',
            data:{
                school_name:this.data.searchVal
            }
          }).then(res=>{
              console.log('res',res)
            let schoolList = []
            let tab = {}
            const school = res.result.data

            if(res.result.data.length>0){
                console.log('school',school)
                school.forEach((item) => {
                    if (tab[item._id]&&tab[item._id].schoolInfos) {
                      item.num = 0
                      tab[item._id].schoolInfos.push(item)
                    } else {
                      tab[item._id] = {
                      schoolProvince: item.school_province,
                      schoolInfos: []
                      }
                    item.num = 0
                    tab[item._id].schoolInfos.push(item)
                    }
                })
                for(let key in tab) {
                    schoolList.push({
                      _id: key,
                      schoolInfos: tab[key].schoolInfos,
                      schoolProvince: tab[key].schoolProvince
                    })
                  }
                that.setData({
                    schoolList:schoolList,
                }),wx.hideToast()
            }

          })
    },
    myShowToast(title = '标题') {
        wx.showToast({
          title: title,
          image:'/pages/images/loading.gif',
          size: 700,
          mask: true
        })
      },
})