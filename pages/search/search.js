Page({
  data:{
    catlist:[]
  },
  onShow(){
      var that = this
  },
    // 搜索栏输入名字后页面跳转
    bindconfirmT: function (e) {
      var that = this
      let value = e.detail.value
      if(value){
    this.myShowToast('加载中');
        wx.cloud.callFunction({
          name: 'searchanimals',
          data:{
            search:value,
          }
        }).then(res=>{
        wx.hideToast();
          if(res.result.errMsg == 'collection.get:ok'){
            that.setData({
              catlist:res.result.data
            })
          }
        })
      }
    },
    gocats(e){
      let id = e.currentTarget.dataset.idx
      let animals_id = this.data.catlist[id]._id
      wx.navigateTo({
        url: '../cats/cats?animals_id=' + animals_id,
      })
    },
    myShowToast(title = '标题') {
      wx.showToast({
        title: title,
        image:'/pages/images/loading.gif',
        size: 700,
        mask: true
      })
    }
})