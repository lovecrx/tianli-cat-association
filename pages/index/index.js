var app = getApp()
Page({
data: { 
 cats: [],
 dogs: [],
 others: [],
 pets:[],
    // 加载相关
    loading: false, // 正在加载
    loadnomore: false, // 没有再多了
    showTodayTip:true,
cat_status_adopt: ["未领养", "已领养", "寻找领养中"],
animals_popularity:'人气动物',
configtext:{
  dogdeath:'汪！俺返回汪星啦~',
  catdeath:'喵！俺返回喵星啦~',
  otherdeath:'小主人,再见啦！'
},
    screenWidth: 0,
    screenHeight: 0,
    imgwidth: 0,
    imgheight: 0,
    navbar: ['猫咪', '汪汪', '其他','宠物'],
    currentTab: 0,
    url: app.globalData.url,
    schoolrange:[
        {schoolname:'按照欢迎度升序',type:0,all_active:false},
        {schoolname:'按照欢迎度降序',type:1,all_active:false},
        {schoolname:'等待领养中',type:2,all_active:false},
        {schoolname:'毕业的小动物',type:3,all_active:false},
        {schoolname:'折叠信息',type:4,all_active:false},
    ],
    orderBy:'desc',
    adopt:false,
    death:false,
    flode:true

  },
  navbarTap: function (e) {
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    }),this.getanimals();
  },
  onShow:function(){
      this.getanimals(),this.getpicture()
  },
  getpicture(){
      var that = this
    wx.cloud.callFunction({
        name:'getpicturestatus',
      }).then(res=>{
          console.log('res',res.result.data[0])
          that.setData({
            help_picture:res.result.data[0].picture,
            pictureshow:res.result.data[0].pictureshow,
            pay_photo:res.result.data[0].pay_photo
          })
      })
  },
  getanimals(){
    var that = this
    this.myShowToast('加载中');
    wx.cloud.callFunction({
      name:'getanimalsstatus',
      data:{
        currentTab:that.data.currentTab,
        orderBy:that.data.orderBy
      }
    }).then(res=>{
      if(res.result.errMsg == 'collection.get:ok'){
        wx.hideToast();
        if(that.data.currentTab == 0){
            let cats = res.result.data
            if(that.data.adopt){
                let res = cats.filter((x) => {
                    return x.adopt == '2';
                  });
                  that.setData({
                    cats:res,
                    loadnomore:true
                  })
                return;
            }else if(that.data.death){
                let res = cats.filter((x) => {
                    return x.death == true;
                  });
                  that.setData({
                    cats:res,
                    loadnomore:true
                  })
                return;
            }else{
                that.setData({
                    cats:res.result.data,
                    loadnomore:true
                  })
                return;
            }
        }
        if(that.data.currentTab == 1){
            let dogs = res.result.data
            if(that.data.adopt){
                let res = dogs.filter((x) => {
                    return x.adopt == '2';
                  });
                  that.setData({
                    dogs:res,
                    loadnomore:true
                  })
                return;
            }else if(that.data.death){
                let res = dogs.filter((x) => {
                    return x.death == true;
                  });
                  that.setData({
                    dogs:res,
                    loadnomore:true
                  })
                return;
            }else{
                that.setData({
                    dogs:res.result.data,
                    loadnomore:true
                })
                return;
            }
        }
        if(that.data.currentTab == 2){
            let others = res.result.data
            if(that.data.adopt){
                let res = others.filter((x) => {
                    return x.adopt == '2';
                  });
                  that.setData({
                    others:res,
                    loadnomore:true
                  })
                return;
            }else if(that.data.death){
                let res = others.filter((x) => {
                    return x.death == true;
                  });
                  that.setData({
                    others:res,
                    loadnomore:true
                  })
                return;
            }else{
                that.setData({
                    others:res.result.data,
                    loadnomore:true
                })
                return;
            }
        }
        if(that.data.currentTab == 3){
            let pets = res.result.data
            if(that.data.adopt){
                let res = pets.filter((x) => {
                    return x.adopt == '2';
                  });
                  that.setData({
                    pets:res,
                    loadnomore:true
                  })
                return;
            }else if(that.data.death){
                let res = pets.filter((x) => {
                    return x.death == true;
                  });
                  that.setData({
                    pets:res,
                    loadnomore:true
                  })
                return;
            }else{
                that.setData({
                    pets:res.result.data,
                    loadnomore:true
                })
                return;
            }
        }
      }
    })
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },

  //转发跳转页面设置
  onLoad: function (options) {
    if (options.pageId) {
      wx.navigateTo({
        url: '/pages/cats/' + options.pageId + '/' + options.pageId,
      })
    }
  },

  //转发此页面的设置
  onShareAppMessage: function (ops) {
    if (ops.from === 'button') {
      // 来自页面内转发按钮
      console.log(ops.target)
    }
    return {
      path: 'pages/index/index',  // 路径，传递参数到指定页面。
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }
  },

  // 转发到朋友圈
  onShareTimeline: function (res) {
    if (ops.from === 'button') {
      // 来自页面内转发按钮
      console.log(ops.target)
    }
    return {
      path: 'pages/index/index',  // 路径，传递参数到指定页面。
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }
  },

  // 搜索栏输入名字后页面跳转
  bindconfirmT: function (e) {
    
    this.myShowToast('加载中');
    wx.navigateTo({
      url: '../search/search',
    })
  },
  copyTBL: function (e) {
    var self = this;
    wx.setClipboardData({
      data: '理工动物协',//需要复制的内容
      success: function (res) {
        // self.setData({copyTip:true}),

      }
    })
  },
  goanimals(e){
    let animalscate = e.currentTarget.dataset.url
    wx.navigateTo({
      url: './animals/animals?category=' +animalscate,
    })
  },
  gocats(e){
    let animals_id = e.currentTarget.dataset._id
    this.myShowToast('加载中');
    wx.navigateTo({
      url: '../cats/cats?animals_id=' + animals_id,
    })
  },
    /**
   * 页面上拉触底事件的处理函数
   */

  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  selectschool(e){
      wx.navigateTo({
        url: '../getschooladdress/getschooladdress',
      })
  },
  myShowToast(title = '标题') {
    wx.showToast({
      title: title,
      image:'/pages/images/loading.gif',
      size: 700,
      mask: true
    })
  },
  changeToggle(){
    var that = this;
    that.setData({
      isShow:!that.data.isShow
    })
  },
  sortanimals(e){
      console.log('e',e)
      let idx = e.currentTarget.dataset.index
      let type = e.currentTarget.dataset.type
      var schoolrange = this.data.schoolrange
      this.clear()
      schoolrange[idx].all_active = !schoolrange[idx].all_active
      if(type == 0){
          this.setData({
              death:Boolean(false),
              adopt:Boolean(false),
              orderBy:'asc',
              schoolrange:schoolrange
          })
      }else if(type == 1){
          this.setData({
              death:Boolean(false),
              orderBy:'desc',
              adopt:Boolean(false),
              schoolrange:schoolrange
          })
      }else if(type == 2){
          this.setData({
              death:Boolean(false),
              orderBy:'desc',
              adopt:Boolean(true),
              schoolrange:schoolrange
          })
      }else if(type == 3){
        this.setData({
            orderBy:'desc',
            death:Boolean(true),
            adopt:Boolean(false),
            schoolrange:schoolrange
        })
      }
      else if(type == 4){
          console.log('this.data.schoolrange',this.data.schoolrange)
        let flode = Boolean
        flode = !this.data.flode
        this.setData({
            flode:flode,
            schoolrange:schoolrange

        })
      }
      this.getanimals()
  },
  clear(){
    var schoolrange = this.data.schoolrange
    schoolrange[0].all_active = Boolean(false)
    schoolrange[1].all_active = Boolean(false)
    schoolrange[2].all_active = Boolean(false)
    schoolrange[3].all_active = Boolean(false)
  },
  enlargeimage(){
    let Arraylist = ["/pages/images/huihui.png"]
    wx.previewImage({
    current: [0], // 当前显示图片的http链接
    urls: Arraylist // 需要预览的图片http链接列表
    })
  },
    // 弹出发布选择框
    showModal:function(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
  assistance(){
    wx.previewImage({
      current: [0], // 当前显示图片的http链接
      urls: this.data.help_picture // 需要预览的图片http链接列表
    })
  },
  donation(e){
    wx.previewImage({
        current: [0], // 当前显示图片的http链接
        urls: [this.data.pay_photo] // 需要预览的图片http链接列表
      })
  },
  paydetail(e){
      wx.navigateTo({
        url: '../paydetail/paydetail',
      })
  }
})

