var app = getApp()
Page({
data: { 
    screenWidth: 0,
    screenHeight: 0,
    imgwidth: 0,
    imgheight: 0,
    catlist:[]
  },

  onPullDownRefresh:function(){
    wx.stopPullDownRefresh()
  },

  //转发跳转页面设置
  onLoad: function (options) {
    var that = this
    this.myShowToast('加载中');
    if(options.category){
      wx.cloud.callFunction({
        name: 'getanimalsCategory',
        data:{
          category:options.category
        }
      }).then(res=>{
        wx.hideToast();
        if(res.result.errMsg == 'collection.get:ok'){
          that.setData({
            catlist:res.result.data
          })
        }
      })
    }
    if (options.pageId) {
      
      wx.navigateTo({
        url: '/pages/cats/' + options.pageId + '/' + options.pageId,
      })
    }
  },

  //转发此页面的设置
  onShareAppMessage: function (ops) {
    if (ops.from === 'button') {
      // 来自页面内转发按钮
      console.log(ops.target)
    }
    return {
      path: 'pages/index/index',  // 路径，传递参数到指定页面。
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }
  },

  // 搜索栏输入名字后页面跳转
  bindconfirmT: function (e) {
    console.log("e.detail.value");
    if(e.detail.value) {
    
    wx.navigateTo({
      url: '/pages/cats/' + e.detail.value + '/' + e.detail.value,
    })
  }
  },
  gocats(e){
    let id = e.currentTarget.dataset.idx
    let animals_id = this.data.catlist[id]._id
    wx.navigateTo({
      url: '../../cats/cats?animals_id=' + animals_id,
    })
  },
  myShowToast(title = '标题') {
    wx.showToast({
      title: title,
      image:'/pages/images/loading.gif',
      size: 700,
      mask: true
    })
  }
})

