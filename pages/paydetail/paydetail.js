// pages/paydetail/paydetail.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        currentTab: 0,
        navbar: ['收入', '支出'],
        listData:[]
    },
    navbarTap: function (e) {
        this.setData({
          currentTab: e.currentTarget.dataset.idx
        }),this.getpaydetail(this.data.currentTab)
      },
      onShow(){
          this.getpaydetail(this.data.currentTab)
      },
      getpaydetail(type){
        console.log('res.result.data',type.toString())

        var that = this
        this.myShowToast('加载中');
        wx.cloud.callFunction({
          name: 'getpaydetail',
          data:{
            pay_type:type.toString()
          }
        }).then(res=>{
            console.log('res.result.data',res)
            that.setData({
                listData:res.result.data
            })
        })
      },
      myShowToast(title = '标题') {
        wx.showToast({
          title: title,
          image:'/pages/images/loading.gif',
          size: 700,
          mask: true
        })
      },
})