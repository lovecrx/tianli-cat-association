// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
//初始化云函数
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
    try{
        if(event.school_name){
            return await db.collection('schoolrange').where({
                //text为要搜索的对象名，务必对应集合里面的名字
                school_name: db.RegExp({
                    //inputValue为输入框的值，也是就要查询内容，可以自己定义
                    regexp: event.school_name,
                    //大小写不区分
                    options: 'i',
                })
            }).get();
        }else{
            return await db.collection('schoolrange').get();
        }
    }catch(e){
        console.log(e)
    }
}