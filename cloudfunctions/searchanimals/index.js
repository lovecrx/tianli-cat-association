// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
 return await db.collection('animals').where({
    //text为要搜索的对象名，务必对应集合里面的名字
    name: db.RegExp({
      //inputValue为输入框的值，也是就要查询内容，可以自己定义
      regexp: event.search,
      //大小写不区分
      options: 'i',
    }),
  }).get()
}