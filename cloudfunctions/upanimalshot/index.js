// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
//初始化云函数
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  // 返回数据库查询结果
  let _id = event._id
  let popularity = event.popularity + 1
  try{
   return await db.collection('animals').doc(_id).update({
     data:{
      popularity:Number(popularity)
     }
   })
   }catch (e){
     console.log(e)
   }
}