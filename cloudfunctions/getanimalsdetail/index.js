// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
//初始化云函数
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  // 返回数据库查询结果
  try{
   const animals = await db.collection('animals').where({"_id":event._id}).get();
   const album =  await db.collection('photo').where({"animals_id":event._id}).orderBy('_createTime', 'asc').get();
   return{
    animals:animals,
    album:album
   }
  }catch (e){
    console.log(e)
  }
}