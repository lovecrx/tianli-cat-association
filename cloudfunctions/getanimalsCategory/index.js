// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
//初始化云函数
const db = cloud.database()

// 查询数据库集合云函数入口函数
exports.main = async (event, context) => {
  // 返回数据库查询结果
  if(event.category == 'all'){
    return await db.collection('animals').get();
  }else{
    return await db.collection('animals').where({
      animalscate:event.category
    }).get();
  }

};
